package de.novatec.bpm.camunda_cockpit.plugin.resources;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.camunda.bpm.cockpit.plugin.resource.AbstractCockpitPluginRootResource;

import de.novatec.bpm.camunda_cockpit.plugin.SimulationPlugin;
import de.novatec.bpm.camunda_cockpit.plugin.db.ProcessDefinitionResource;

@Path("plugin/"+SimulationPlugin.ID)
public class SimulationPluginRootResource extends AbstractCockpitPluginRootResource {

	public SimulationPluginRootResource() {
		super(SimulationPlugin.ID);
	}
	
	@Path("{engineName}/process-definitions")
	public ProcessDefinitionResource getProcessDefinitionResource(@PathParam("engineName") String engineName) {
		return subResource(new ProcessDefinitionResource(engineName), engineName);
	}

}
