package de.novatec.bpm.camunda_cockpit.plugin.db;

import java.util.List;

import javax.ws.rs.GET;

import org.camunda.bpm.cockpit.db.QueryParameters;
import org.camunda.bpm.cockpit.plugin.resource.AbstractCockpitPluginResource;

public class ProcessDefinitionResource extends AbstractCockpitPluginResource {

	public ProcessDefinitionResource(String engineName) {
		super(engineName);
	}

	@GET
	public List<ProcessDefinitionDto> getProcessDefinitions() {
		QueryParameters<ProcessDefinitionDto> params = new QueryParameters<ProcessDefinitionDto>();
		return getQueryService().executeQuery("novatec.simulation.selectAllProcessDefinitions", params);
	}
}
