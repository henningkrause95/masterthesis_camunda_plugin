package de.novatec.bpm.camunda_cockpit.plugin.db;

public class ProcessDefinitionDto {

	private String key, procDefName;
	private int version;

	public ProcessDefinitionDto(String key, String procDefName, int version) {
		super();
		this.key = key;
		this.procDefName = procDefName;
		this.version = version;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getProcDefName() {
		return procDefName;
	}

	public void setProcDefName(String procDefName) {
		this.procDefName = procDefName;
	}
	
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
}
