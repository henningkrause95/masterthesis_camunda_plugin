package de.novatec.bpm.camunda_cockpit.plugin;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.camunda.bpm.cockpit.plugin.spi.impl.AbstractCockpitPlugin;

import de.novatec.bpm.camunda_cockpit.plugin.resources.SimulationPluginRootResource;

/**
 * This is a Camunda Cockpit plugin to simulate process instances loaded from
 * the Camunda BPM engine database.
 * 
 * @author Hans Henning Krause (HKR)
 * @since Dec 6, 2017
 * @version 1.0-SNAPSHOT
 */
public class SimulationPlugin extends AbstractCockpitPlugin {

	public final static String ID = "simulation-plugin";

	/**
	 * @return ID of plugin
	 */
	public String getId() {
		return ID;
	}

	/**
	 * points to "de/novatec/bpm/camunda_cockpit/plugin/queries/*.xml"
	 * 
	 * @return path to mapping files
	 */
	@Override
	public List<String> getMappingFiles() {
		String path = "de/novatec/bpm/camunda_cockpit/plugin/queries/";
		
		// add mapping files manually for quick+easy results
		List<String> files = new ArrayList<String>();
		files.add(path+"process-definitions.xml");
		return files;
	}

	/**
	 * @return Set with {@link SimulationPluginRootResource}.class in it
	 */
	@Override
	public Set<Class<?>> getResourceClasses() {
		Set<Class<?>> classes = new HashSet<Class<?>>();

		classes.add(SimulationPluginRootResource.class);

		return classes;
	}
}
