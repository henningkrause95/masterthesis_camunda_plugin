// require bpmn-js module
// src: https://github.com/bpmn-io/bpmn-js
//require(["../../../api/cockpit/plugin/simulation-plugin/static/bpmn-js-master/lib/Viewer.js"]);
// TODO: Implement Viewer

define(
		[ "angular"],
		function(angular) {

			var DashboardController = [
					"$scope",
					"$http",
					"$sce",
					"$window",
					"Uri",
					function($scope, $http, $sce, $window, Uri) {

						// ======================================================
						// Custom Plugin Logic resides inside this function
						// ======================================================

						// uri defined by @Path in RootResource
						$http
								.get(
										Uri
												.appUri("plugin://simulation-plugin/:engine/process-definitions"))
								.success(
										function(data) {
											// initial values of fields in controller
											$scope.num = data.length;
											$scope.processDefinitions = data;
											$scope.showing = false;
											$scope.showingResults = false;
											$scope.showResultsBtnText = "Ergebnisse anzeigen";
											$scope.bpmnElements = new Array();
											$scope.sim_instances = 100;

											handleControllerInitialized();
										});
						
						function handleControllerInitialized() {
							// load bpmn20Xml and execution times from REST API
							for (var i = 0; i < $scope.processDefinitions.length; i++) {
								var procDef = $scope.processDefinitions[i];
								procDef.enabled = false;
								getExecutionTime(procDef);
								getBpmnXml(procDef);
							}	
						}

						// function to switch process views
						$scope.onClickSimulieren = function(show, key, version) {
							$scope.bpmnElements = new Array();
							
							// get selected Process definition by Key from ButtonEvent;
							$scope.currentProcess = $scope.processDefinitions.filter(function(e) {
								return (e.key == key && e.version == version)
							})[0];

							// load bpmn diagram display if it needs to be
							// displayed
							if (show) {
								showDetailView();
							} else {
								showMainView();
							}
							$scope.showing = show;
						}
						
						$scope.onClickShowResults = function(show) {
							$scope.showingResults = show;
							if (!$scope.showingResults) {
								$scope.showResultsBtnText = "Ergebnisse anzeigen";
							}
							else {
								showResults(true);
								$scope.showResultsBtnText = "Ergebnisse verbergen";
							}
						}
						
						function showResults() {
							var i = 0, totalTime = 0;
							for (i; i < $scope.simResultList.length; i++) {
								totalTime += $scope.simResultList[i].time;
							}
							$scope.avgSimTime = Math.floor(totalTime / i);
						}
						
						function showDetailView() {
							$scope.showingResults = false;
							displayBpmn($scope.currentProcess.key);
						}
						
						function showMainView() {
							// nothing to do here (yet) since angularjs handles 
							// displaying the main view via via $scope.showing
						}
						
						// handling button clicks of Button with id "start_sim"
						$scope.startSimulation = function(event) {				
							$scope.simulationInProgress = true;	
							$scope.simResultList = Array();
							$scope.endEvents = Array();
							
							var tempEndEvents = getElementsByType("endEvent");
							for(var i=0; i < tempEndEvents.length; i++) {
								$scope.endEvents.push({
									"id" : tempEndEvents[i].id,
									"name" : tempEndEvents[i].name,
									"count" : 0,
									"mean_time" : 0
								});
							}
							
							loadProbabilities();			
							
							// actual simulation code block
							for (var i=0; i < $scope.sim_instances; i++) {
								var nextElement = getRandomStartEvent(), lastElement = null;
								var simulatedTime = 0;
								
								// discover next element in path until no next element given (current = end event)
								while (nextElement != null) {
									// add simulated activity time onto total time of iteration
									simulatedTime += simulateElement(nextElement);									
									// go into next Element
									lastElement = nextElement;									
									// if next element is gateway get next element by probability
									if (nextElement.type == "gateway")
										nextElement = findBpmnElementById(getRandomGatewayCondition(lastElement).target);
									else
										nextElement = findBpmnElementById(lastElement.target);
								}
								// push result of iteration into result list
								$scope.simResultList.push({
									"iteration" : i+1,
									"time" : simulatedTime,
									"endEvent" : lastElement.name
								});
								// counting +1 to amount of End Event
								var endEvent = $.grep($scope.endEvents, function(e) {return e.id == lastElement.id})[0];
								endEvent.count++;
							}
							
							// calculating mean time per endEvent
							for (var i=0; i < $scope.endEvents.length; i++) {
								var eventId = $scope.endEvents[i].id;
								// TODO
							}
							
							// finish up simulation and show results
							$scope.simulationInProgress = false;
							$scope.onClickShowResults(true);
						}
						
						// function to simulate a given BPMN element
						function simulateElement(element) {
							var simulatedTime = 0;
							
							if (element.type == "userTask" || element.type == "callActivity") {
								if (!isNaN(element.mean_time) && element.mean_time != null)
									simulatedTime = generateRandomExecTime(element.mean_time, element.deviation, element.min_time, element.max_time);
							}
							return simulatedTime;
						}
						
						// using "Marsaglia Polar Method"
						var spare = null;
						function generateRandomExecTime(mean, deviation, min, max) {
							var result = 0;
							if (spare != null) {
								result = spare;
								spare = null;
								if (result > min && result < max)
									return result;
							}							
							do {
								var u, v, s;
								do {
									u = Math.random() * 2 - 1;
									v = Math.random() * 2 - 1;
									s = u * u + v * v;
								} while (s >= 1 || s==0);
								
								var muller = Math.sqrt(-2 * Math.log(s) / s);
								result = Math.abs(mean + muller * u * deviation);
								spare = Math.abs(mean + muller * v * deviation);
							} while (result < min || result > max);
							return result;
						}

						// fetch bpmn xml from Rest API (since it is not
						// included in regular Database API)
						function getBpmnXml(procDef) {
							var key = procDef.key;

							// todo: set url in variable for non localhost urls
							jQuery.getJSON(
									"http://localhost:8080/engine-rest/engine/default/process-definition/key/"
											+ key + "/xml/", function(json) {
										procDef.bpmn = json["bpmn20Xml"];
										procDef.enabled = true;
										console.log("Button for " + key
												+ " enabled!")
									});
						}
						
						// get average duration of process
						function getExecutionTime(procDef) {
							console.log("Getting Execution Time for "+procDef.key);
							var apiUrl = "http://localhost:8080/engine-rest/engine/default/history/process-instance?finished=true"
								+ "&processDefinitionKey="+procDef.key;
							
							jQuery.getJSON(apiUrl, function(json) {
								var accumulatedTime = 0;
								for (var i=0; i < json.length; i++) {
									console.log(json[i].processDefinitionVersion +", "+procDef.version);
									if (json[i].state != "COMPLETED" || !(json[i].processDefinitionVersion == procDef.version))
										continue;
									var start = new Date(json[i].startTime);
									var end = new Date(json[i].endTime);
									accumulatedTime += (end - start);
								}
								console.log(accumulatedTime);
								procDef.avgTime = (accumulatedTime / json.length);
							});
						}

						// init bpmn.io component to show bpmn for element with
						// given key
						function displayBpmn(key) {
							// get and parse xml
							console.log("Fetching BPMN XML for: " + key);
							var bpmnXml = $scope.currentProcess.bpmn;
							
							var parser = new DOMParser();
							var procDef = parser.parseFromString(bpmnXml, "text/xml");
							
							parseEvents(procDef, "start");
							parseTasks(procDef, "user");
							parseTasks(procDef, "service");
							parseTasks(procDef, "businessRule");
							parseActivities(procDef);
							parseGateways(procDef);
							parseEvents(procDef, "end");
							
							factorTargetHtml();
						}
						
						// function to build an HTML String to display in column "target"
						function factorTargetHtml() {
							// parse target column for table
							for (var i = 0; i < $scope.bpmnElements.length; i++) {
								var current = $scope.bpmnElements[i];
								
								var target_html = "";
								
								// gateways have extra html elements
								if (current.type == "gateway") {
									for (var j = 0; j < current.conditions.length; j++) {
										var cond_name = current.conditions[j].name;
										var cond_target = current.conditions[j].target;
										
										// TODO: not hardcoded
										target_html += cond_name +" &#8594; "+cond_target+"; Verteilungsgrad: " 
												+ "<input id=\"distribution_"+cond_name+"\" type=\"number\" />"
												+ "<br />";
									}
								} else if (current.type == "startEvent") {
									target_html = "Startwahrscheinlichkeit: <input id=\"probability_"+current.id+"\""
											+ "type=\"number\" />"
											+ "&#8594; "+current.target;
								} else {
									target_html = current.target;
								}					
								current.target_html = $sce.trustAsHtml(target_html);
							}
						}

						// parse gateways from process definition XML DOM
						function parseGateways(procDoc) {
							// get all exclusive Gateways from DOM
							gateways_xml = procDoc
									.getElementsByTagName("exclusiveGateway");
							
							// remember range of gateway elements in array for pushing conditions later on
							var startIndex = $scope.bpmnElements.length;
							
							// get name of gateway and factor empty object
							for (var i = 0; i < gateways_xml.length; i++) {
								$scope.bpmnElements.push({
									"name" : gateways_xml[i]
											.getAttribute("name"),
									"id" : gateways_xml[i].getAttribute("id"),
									"type" : "gateway",
									"conditions" : []
								});
								
								// push gateway conditions into gateway.conditions array
								for (var n = 0; n < gateways_xml[i].childNodes.length; n++) {
									// select only "outgoing" child nodes ( = sequence flows)
									var currentNode = gateways_xml[i].childNodes[n];
									if (gateways_xml[i].childNodes[n].nodeName == "outgoing") {
										// get name of condition from gateway xml
										var condition_name = currentNode.childNodes[0].nodeValue;
										
										// load attached conditions to gateway from adjacent sequence flows
										var outgoingSF = getSequenceFlow(procDoc, condition_name);
										var expression = outgoingSF.childNodes[1].childNodes[0].nodeValue;
										var target = outgoingSF.getAttribute("targetRef");
										
										// push conditions object with name and expression into gateways array
										$scope.bpmnElements[startIndex + i].conditions.push({
											"name": condition_name,
											"expression": expression,
											"target" : target
										});
									}
								}
							}
						}
						
						function parseEvents(procDoc, type) {
							var event_type = type + "Event";
							var events_xml = procDoc.getElementsByTagName(event_type);
							
							for (var i=0; i < events_xml.length; i++) {
								var currentEv = events_xml[i];	
								$scope.bpmnElements.push({
									"name" : currentEv.getAttribute("name"),
									"id" : currentEv.getAttribute("id"),
									"type" : event_type
								});
								// startEvents have extra fields
								if (type == "start") {
									var index = $scope.bpmnElements.length-1;
									$scope.bpmnElements[index].target = getTargetElement(procDoc, currentEv); // only startEvents have targets				
									$scope.bpmnElements[index].probability = 0;
								}
							}
						}
						
						function parseTasks(procDoc, type) {
							var task_type = type + "Task";
							var tasks_xml = procDoc.getElementsByTagName(task_type);
							
							for (var i=0; i < tasks_xml.length; i++) {
								var currentTask = tasks_xml[i];
								
								calculateStatisticalFeatures(currentTask, "task");
								
								$scope.bpmnElements.push({
									"name" : currentTask.getAttribute("name"),
									"id" : currentTask.getAttribute("id"),
									"type" : task_type,
									"target" : getTargetElement(procDoc, currentTask)
								});
							}
						}
						
						function parseActivities(procDoc) {
							var activities_xml = procDoc.getElementsByTagName("callActivity");
							
							for (var i=0; i < activities_xml.length; i++) {
								var currentAct = activities_xml[i];
								
								calculateStatisticalFeatures(currentAct, "activity");								
								
								$scope.bpmnElements.push({
									"name" : currentAct.getAttribute("name"),
									"id" : currentAct.getAttribute("id"),
									"type" : "callActivity",
									"target" : getTargetElement(procDoc, currentAct)
								});
							}
						}
						
						// util function to retrieve a sequence flow element by it"s id
						function getSequenceFlow(procDef, id) {
							var seqflows_xml = procDef.getElementsByTagName("sequenceFlow");
							var sf;
							
							for (var i=0; i<seqflows_xml.length; i++) {
								if (seqflows_xml[i].getAttribute("id") == id) {
									sf = seqflows_xml[i];
									break;
								}
							}
							return sf;
						}
						
						// util function get target, an outgoing flow of a source element is pointing to
						function getTargetElement(procDef, srcElem) {
							var target;
							for (var i=0; i < srcElem.childNodes.length; i++) {
								if (srcElem.childNodes[i].nodeName == "outgoing") {
									var outgoing_name = srcElem.childNodes[i].childNodes[0].nodeValue;
									target = getSequenceFlow(procDef, outgoing_name).getAttribute("targetRef");
									break;
								}
							}
							return target;
						}
						
						// util function to calculate average execution time of given task
						function calculateStatisticalFeatures(element, type) {
							if (type=="task") {
								var apiUrl = "http://localhost:8080/engine-rest/engine/default/history/"
									+ "task?taskDefinitionKey="+element.getAttribute("id")
									+ "&deleteReason=completed";
								
								// getting task durations from REST API
								jQuery.getJSON(apiUrl, function(json) {
									var i=0, total=0, max, min;
									for(i; i<json.length; i++) {
										var duration = json[i].duration;
										total += duration;
										if (max == null || duration > max) {
											max = duration;
										}
										if (min == null || duration < min) {
											min = duration;
										}
									}
									var mean = Math.floor(total / i), deviation=0, n=1;
									for (n; n<json.length+1; n++) {
										deviation += Math.pow((json[n-1].duration - mean), 2);
									}
									var deviation = Math.floor(Math.sqrt(deviation / (n-1)));
									
									// putting avg time into respective task object
									var result = $.grep($scope.bpmnElements, function(e) {return e.id == element.getAttribute("id")});
									result[0].mean_time = mean; // only 1 element can be inside array, hence result[0]
									result[0].min_time = min; 
									result[0].max_time = max; 
									result[0].deviation = deviation; 
									
									$scope.$digest(); //apply data changes to UI manually because calculation executed asynchronously
								});
							}
							if (type=="activity") {
								var apiUrl = "http://localhost:8080/engine-rest/engine/default/history/"
									+ "activity-instance?activityId="+element.getAttribute("id")
									+ "&completeScope=true";
								
								jQuery.getJSON(apiUrl, function(json) {
									var i=0, total=0, max, min;
									for (i; i<json.length; i++) {
										var duration = json[i].durationInMillis;
										total += duration;
										if (max == null || duration > max) {
											max = duration;
										}
										if (min == null || duration < min) {
											min = duration;
										}
									}
									var mean = Math.floor(total / i), deviationPart=0, n=1;
									for (n; n<json.length+1; n++) {
										deviationPart += Math.pow((json[n-1].durationInMillis - mean), 2);
									}
									var deviation = Math.floor(Math.sqrt(deviationPart / (n-1)));
									
									var result = $.grep($scope.bpmnElements, function(e) {return e.id == element.getAttribute("id")})[0];
									result.mean_time = mean;
									result.min_time = min; 
									result.max_time = max; 
									result.deviation = deviation;
									
									$scope.$digest();
								});
							}
						}
						
						function getRandomStartEvent() {
							var weighted_list = Array();
							var startEventArr = getElementsByType("startEvent");
							for (var i=0; i < startEventArr.length; i++) {
								for (var c=0; c < startEventArr[i].probability; c++) {
									weighted_list.push(startEventArr[i]);
								}
							}
							var random = Math.floor(Math.random() * (weighted_list.length - 1) + 1);
							return weighted_list[random];
						}
						
						// derived from above function
						function getRandomGatewayCondition(gateway) {
							var weighted_list = Array();
							var conditions = gateway.conditions;
							for (var i=0; i < conditions.length; i++) {
								for (var c=0; c < conditions[i].probability; c++) {
									weighted_list.push(conditions[i]);
								}
							}
							var random = Math.round(Math.random() * (weighted_list.length - 1));
							return weighted_list[random];
						}
						
						// util function to retrieve and save probabilities from input fields 
						function loadProbabilities() {
							for (var i=0; i < $scope.bpmnElements.length; i++) {
								if ($scope.bpmnElements[i].type == "startEvent") {
									$scope.bpmnElements[i].probability = $("#probability_"+$scope.bpmnElements[i].id).val();
								}
								if ($scope.bpmnElements[i].type == "gateway") {
									var gateway = $scope.bpmnElements[i];
									for (var c=0; c < gateway.conditions.length; c++) {
										gateway.conditions[c].probability = $("#distribution_"+gateway.conditions[c].name).val();
									}
								}
							}
						}
						
						// util function to retrieve array of bpmn elements matching the given type
						function getElementsByType(type) {
							return $scope.bpmnElements.filter(function(e) {
								return e.type == type;
							});
						}
						
						// util function to find an element inside $scope.bpmnElements with the given key
						function findBpmnElementById(id) {
							return $scope.bpmnElements.filter(function(e) {
								return e.id == id;
							})[0];
						}

						// returns process definition with a certain key from
						// given array
						function findByKey(key, procDefArr) {
							console.log(procDefArr);
							for (var i = 0; i < procDefArr.length; i++) {
								var def = procDefArr[i];
								if (def.key == key) {
									return def;
								}
							}
							return null;
						}
						
						// util function to format millisecond values into time string
						$scope.toTimeString = function(millis) {							
							// catch faulty values
							if(millis == null)
								return " - ";
							if(isNaN(millis))
								return "00:00:00";
								
							var seconds = millis / 1000;
							var h = Math.floor(seconds / 3600);
							var m = Math.floor((seconds - (h*3600)) / 60);
							var s = Math.floor(seconds - (h * 3600) - (m * 60));
							
							if (h < 10) h = "0"+h;
							if (m < 10) m = "0"+m;
							if (s < 10) s = "0"+s;
							
							return h+":"+m+":"+s;
						}
					}];

			// Configuration to publish Plugin to Cockpit
			var Configuration = function Configuration(ViewsProvider) {

				ViewsProvider
						.registerDefaultView(
								"cockpit.dashboard",
								{
									id : "cockpit-dashboard-simulation",
									label : "Simulation Plugin",

									// full url
									// "localhost:8080/camunda/api/cockpit/plugin/{plugin-id}/static/app/*"
									url : "plugin://simulation-plugin/static/app/dashboardView.html",
									controller : DashboardController,

									// making sure we have a higher priority
									// than default plugin
									priority : 12
								});
			};

			var module = angular.module("cockpit.plugin.simulation-plugin", []);
			module.config(Configuration);
			return module;
		});