package de.novatec.bpm.camunda_cockpit.plugin;

import java.util.List;

import org.camunda.bpm.cockpit.Cockpit;
import org.camunda.bpm.cockpit.db.QueryParameters;
import org.camunda.bpm.cockpit.db.QueryService;
import org.camunda.bpm.cockpit.plugin.spi.CockpitPlugin;
import org.camunda.bpm.cockpit.plugin.test.AbstractCockpitPluginTest;
import org.junit.Assert;
import org.junit.Test;

import de.novatec.bpm.camunda_cockpit.plugin.db.ProcessDefinitionDto;

public class SimulationPluginTest extends AbstractCockpitPluginTest {

	@Test
	public void testPluginDiscovery() {
		CockpitPlugin plugin = Cockpit.getRuntimeDelegate().getAppPluginRegistry().getPlugin("simulation-plugin");
		Assert.assertNotNull(plugin);
	}
	
	@Test
	public void testProcessDefinitionQueryWorking() {
		QueryService qs = getQueryService();
		List<ProcessDefinitionDto> list = qs.executeQuery("novatec.simulation.selectAllProcessDefinitions",
				new QueryParameters<ProcessDefinitionDto>());
		Assert.assertEquals(0, list.size());
	}
}
